import 'package:aplicacionflutter/model/character.dart';

final characters = [
  Character(
    name: 'Juan Sebastian',
    age: 25,
    image: 'images/hombre1.jpg',
    jobTitle: 'Kotlin Developer',
    stars: 3.2,
  ),
  Character(
    name: 'Carmen Bazurto',
    age: 28,
    image: 'images/mujer1.png',
    jobTitle: 'Developer',
    stars: 1.9,
  ),
  Character(
    name: 'Elvis Delgado',
    age: 39,
    image: 'images/hombre2.png',
    jobTitle: 'Developer Web',
    stars: 5.9,
  ),
  Character(
    name: 'Camila Santana',
    age: 28,
    image: 'images/mujer2.png',
    jobTitle: 'Php Developer',
    stars: 2.7,
  ),
  Character(
    name: 'Santiago Solorzano',
    age: 34,
    image: 'images/hombre3.png',
    jobTitle: 'Designer',
    stars: 4.6,
  ),
  Character(
    name: 'Veronica Castro',
    age: 29,
    image: 'images/mujer3.png',
    jobTitle: 'Gestion de proyectos SGI',
    stars: 1.5,
  ),
  Character(
    name: 'Camilo Sandoval',
    age: 44,
    image: 'images/hombre4.png',
    jobTitle: 'Frontend Developer',
    stars: 4.1,
  ),
  Character(
    name: 'Rebeca Rojas',
    age: 26,
    image: 'images/mujer4.png',
    jobTitle: 'Project Manager',
    stars: 3.6,
  ),
  Character(
    name: 'Karola Cevallos',
    age: 28,
    image: 'images/mujer5.png',
    jobTitle: 'Developer',
    stars: 4.5,
  ),
  Character(
    name: 'Daniel Arteaga',
    age: 20,
    image: 'images/hombre5.png',
    jobTitle: 'Design',
    stars: 4.4,
  ),
];
